<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $primaryKey = 'post_id';
    protected $fillable = [
        'user_id',
        'post_title',
        'post_description',
        'post_image',
        'post_status'
    ];

    public function user () {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    public function favorites () {
        return $this->hasMany(Favorite::class, 'post_id', 'post_id');
    }
}