<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Exception;

class UserController extends Controller
{
    public function store (Request $request) {
        try {

            $newUser = new User();
            $newUser->fill([
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'user_type' =>$request->user_type
            ])->save();
            return response()->json([
                'status' => 201,
                'title' => 'Success',
                'message' => 'Store User',
                'user' => $newUser
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  => 400,
                'title'   => 'Error',
                'message' => 'Error al guardar',
                'type'    => 'error',
                'error'   => $e->getMessage()
            ]);
        }
    }
}
