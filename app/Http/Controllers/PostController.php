<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Post;
use App\User;
use App\Favorite;
use Exception;

class PostController extends Controller
{
    public function store (Request $request) {
        try {
            // Image Handling Start
            $carbon = new Carbon();
            $imageFile = $request->file('post_image');
            $imageName = md5($carbon->timestamp.rand()).'.'.$imageFile->extension();
            $folderPath = '/assets/images/';
            $publicPath = public_path().$folderPath;
            $imageFile->move($publicPath, $imageName);
            // Image Handling End

            $newPost = new Post();
            $newPost->fill([
                'post_title' => $request->input('post_title'),
                'post_description' => $request->input('post_description'),
                'post_image' => $imageName,
                'post_status' => $request->input('post_status'),
                'user_id' => $request->input('user_id'),
            ])->save();
            return response()->json([
                'status' => 201,
                'title' => 'Success',
                'message' => 'Post Stores',
                'data' => $request->input('post_title')
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  => 400,
                'title'   => 'Error',
                'message' => 'Error al guardar',
                'type'    => 'error',
                'error'   => $e->getMessage()
            ]);
        }
    }

    public function index () {
        try {
            $posts = Post::where('post_status', 1)->paginate(20);
            foreach ($posts as $post) {
                if ($post->user_id) {
                    $user = User::find($post->user_id);
                    $post->user = $user;
                }
                $fav = Favorite::where('post_id', $post->post_id)->count();
                $post->fav = $fav;
            }
            return response()->json([
                'status' => 200,
                'title' => 'Success',
                'message' => 'Post Get',
                'data' => $posts
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  => 400,
                'title'   => 'Error',
                'message' => 'Error al guardar',
                'type'    => 'error',
                'error'   => $e->getMessage()
            ]);
        }
    }

    public function moderate () {
        try {
            $posts = Post::where('post_status', 0)->get();
            return response()->json([
                'status' => 200,
                'title' => 'Success',
                'message' => 'Post Get',
                'data' => $posts
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  => 400,
                'title'   => 'Error',
                'message' => 'Error al guardar',
                'type'    => 'error',
                'error'   => $e->getMessage()
            ]);
        }
    }

    public function allow (Request $request) {
        try {
            $post = Post::find($request->post_id);
            $post->post_status = 1;
            $post->save();
            return response()->json([
                'status' => 201,
                'title' => 'Success',
                'message' => 'Post Get',
                'data' => $post
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  => 400,
                'title'   => 'Error',
                'message' => 'Error al guardar',
                'type'    => 'error',
                'error'   => $e->getMessage()
            ]);
        }
    }

    public function favorite (Request $request) {
        try {
            $type = 0;
            $check = Favorite::where('post_id', $request->post_id)->where('user_id', $request->user_id)->first();

            if ($check === null) {
                $fav = new Favorite();
                $fav->fill([
                    'post_id' => $request->post_id,
                    'user_id' => $request->user_id
                ]);
                $fav->save();
                $type = 1;
            } else {
                $fav = Favorite::where('post_id', $request->post_id)->where('user_id', $request->user_id)->first();
                $fav->delete();
                $type = 0;
            }
            return response()->json([
                'status' => 201,
                'title' => 'Success',
                'message' => 'Post Get',
                'data' => $check,
                'type' => $type
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  => 400,
                'title'   => 'Error',
                'message' => 'Error al guardar',
                'type'    => 'error',
                'error'   => $e->getMessage()
            ]);
        }
    }

    public function getFavorites (Request $request) {
        try {
            $posts = Favorite::where('user_id', $request->user_id)->with('posts.user')->get();
            return response()->json([
                'status' => 200,
                'title' => 'Success',
                'message' => 'Post Get',
                'data' => $posts
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  => 400,
                'title'   => 'Error',
                'message' => 'Error al guardar',
                'type'    => 'error',
                'error'   => $e->getMessage()
            ]);
        }
    }
}
