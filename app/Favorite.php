<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $primaryKey = 'favorite_id';
    protected $fillable = [
        'post_id',
        'user_id'
    ];

    public function posts () {
        return $this->belongsTo(Post::class, 'post_id', 'post_id');
    }

    public function users () {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
